package com.example.payme.classes;

import com.example.payme.R;

public class Item {
	String item_name = null;
	String address = null;
	int icon = R.drawable.market;
	int status = R.drawable.mobile;
	double distance = 0.0;

	public Item(String name, String address, int icon, int status, double distance) {
		this.item_name = name;
		this.address = address;
		this.icon = icon;
		this.status = status;
		this.distance = distance;
	}

	public String getItem_name() {
		return item_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
}
