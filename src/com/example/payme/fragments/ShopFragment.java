package com.example.payme.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.payme.MainActivity;
import com.example.payme.R;
import com.example.payme.adapters.ShopAdapter;
import com.example.payme.classes.Item;

public class ShopFragment extends Fragment {
	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	private ArrayList<com.example.payme.classes.Item> items = new ArrayList<com.example.payme.classes.Item>();

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static ShopFragment newInstance(int sectionNumber) {
		ShopFragment fragment = new ShopFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public ShopFragment() {
		items.add(new Item("Farment's Market ", "85 Summer St.",
				R.drawable.market, R.drawable.mobile, 0.1));
		items.add(new Item("Juice O Rama ", "2830 N Market St.",
				R.drawable.fruit, R.drawable.mobile, 0.4));
		items.add(new Item("Burger Bistro ", "85 Main St.", R.drawable.bb,
				R.drawable.timeout, 0.5));
		items.add(new Item("Sky's Guitar ", "181 Spring Ave.",
				R.drawable.guitar, R.drawable.mobile, 1.0));
		items.add(new Item("Luma, Ltd ", "75 Lincoln Dr.", R.drawable.color,
				R.drawable.mobile, 1.2));
		items.add(new Item("Dominican Joe ", "515 S Congress Ave.",
				R.drawable.coffee, R.drawable.mobile, 1.8));

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.shop, container, false);

		ListView listView1 = (ListView) rootView.findViewById(R.id.listView1);
		ShopAdapter adapter = new ShopAdapter(getActivity()
				.getApplicationContext(), items);
		listView1.setAdapter(adapter);

		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(getArguments().getInt(
				ARG_SECTION_NUMBER));
	}
}
