package com.example.payme.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.payme.R;

public class ShopAdapter extends
		android.widget.ArrayAdapter<com.example.payme.classes.Item> {
	private ArrayList<com.example.payme.classes.Item> items = null;

	public ShopAdapter(Context context, ArrayList<com.example.payme.classes.Item> objects) {
		super(context, R.layout.shop_item, android.R.id.text1, objects);
		this.items = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View itemView = convertView;
		if (itemView == null) {
			itemView = LayoutInflater.from(getContext()).inflate(
					R.layout.shop_item, parent, false);
		}
		ImageView shop_item_imageview = (ImageView) itemView
				.findViewById(R.id.shop_item_imageview);
		shop_item_imageview.setImageResource(items.get(position).getIcon());

		TextView shop_item_item_name = (TextView) itemView
				.findViewById(R.id.shop_item_item_name);
		shop_item_item_name.setText(items.get(position).getItem_name());

		ImageView shop_item_status_icon = (ImageView) itemView
				.findViewById(R.id.shop_item_status_icon);
		shop_item_status_icon.setImageResource(items.get(position).getStatus());

		TextView shop_item_item_location = (TextView) itemView
				.findViewById(R.id.shop_item_item_location);
		shop_item_item_location.setText(items.get(position).getAddress());

		TextView shop_item_item_distance = (TextView) itemView
				.findViewById(R.id.shop_item_item_distance);
		shop_item_item_distance.setText("" + items.get(position).getDistance()
				+ " mi");

		return itemView;
	}

}
