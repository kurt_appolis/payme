package com.example.payme.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.payme.R;

public class NavAdapter extends android.widget.ArrayAdapter<String> {
	private String[] objects = null;
	private int[] images = { R.drawable.shop, R.drawable.activity,
			R.drawable.transfer, R.drawable.wallet, R.drawable.settings };

	public NavAdapter(Context context, int resource, int textViewResourceId,
			String[] strings) {
		super(context, R.layout.fragment_navigation_drawer_item,
				android.R.id.text1, strings);
		this.objects = strings;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View itemView = convertView;
		if (itemView == null) {
			itemView = LayoutInflater.from(getContext()).inflate(
					R.layout.fragment_navigation_drawer_item, parent, false);
		}
		TextView fragment_navigation_drawer_item_textview = (TextView) itemView
				.findViewById(R.id.fragment_navigation_drawer_item_textview);
		fragment_navigation_drawer_item_textview.setText(objects[position]);

		ImageView fragment_navigation_drawer_item_imageview = (ImageView) itemView
				.findViewById(R.id.fragment_navigation_drawer_item_imageview);
		fragment_navigation_drawer_item_imageview
				.setImageResource(images[position]);

		return itemView;
	}

}
